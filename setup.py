import os

from setuptools import setup, find_packages
from setuptools.extension import Extension
from Cython.Build import cythonize
import numpy

__version__ = '0.1.0'


ext = Extension(
    "dipole.field",
    sources=[os.path.join(os.getcwd(), 'dipole', 'cpp', 'field.cpp')],
    include_dirs=[os.path.join(os.getcwd(), 'dipole')],
    language='c++',
    extra_compile_args=['-std=c++11', '-fopenmp'],
    extra_link_args=['-lgomp'],
)

setup(
    author='Thomas Hisch',
    author_email='t.hisch@gmail.com',
    name='dipole',
    packages=find_packages(),
    ext_modules=cythonize([ext]),
    include_dirs=[numpy.get_include()],
    platforms='Any',
    requires=['python (>=3.4.0)'],
    version=__version__,
)

[![Build Status](https://travis-ci.org/thisch/pydipole.svg?branch=master)](http://travis-ci.org/thisch/pydipole)

Electric dipole radiation in near and far field
===============================================

The electric and the magnetic field of a set of non-interacting radiating
electric dipoles in vacuum are given by

```math
\Large \mathbf{E}(\mathbf{r}, t) = \sum_{n=1}^{N}\frac{1}{4\pi\epsilon_{0}}\left\{ 
   \frac{k^{2}}{r'}(\mathbf{\hat{r}}' \times \mathbf{p}_{n}) \times \mathbf{\hat{r}}' + 
   \frac{1}{{r'}^{3}}(1-ikr')\left[3\mathbf{\hat{r}}'(\mathbf{\hat{r}}'\cdot\mathbf{p}_{n}) - 
   \mathbf{p}_{n}\right]\right\}\,e^{ikr' - i(\omega t + \varphi_{n})}
```
```math
\Large \mathbf{H}(\mathbf{r}, t) = \sum_{n=1}^{N}\frac{ck^2}{4\pi r'} 
(\mathbf{\hat{r}}' \times \mathbf{p}_{n}) \left (1 - \frac{1}{ikr'}\right) 
e^{ikr' - i(\omega t + \varphi_{n})}
```
where $`\mathbf{p}_n`$ are the dipole moments and $`\varphi_n`$ are the temporal phases of
the oscillating dipoles. The quantities $`r'`$ and $`\mathbf{\hat{r}}'`$ depend on the
positions of the dipoles $`\mathbf{a}_n`$,

```math
\Large r' := |\mathbf{r} - \mathbf{a}_{n}|\quad\mathbf{\hat{r}}' := \frac{\mathbf{r} - \mathbf{a}_{n}}{|\mathbf{r} - \mathbf{a}_{n}|}
```

The function ``dipole.field.dipole_general`` evaluates the electric and/or
magnetic field of a set of oscillating dipoles at specified observation
points. If only the far field is of interest, the optimized function
``dipole.field.dipole_e_ff`` can be used.  In the far field limit the fields
are given by

```math
\Large
\mathbf{E}_{\infty}(\mathbf{r}, t) = \sum_{n=1}^{N}\frac{k^{2}}{4\pi\epsilon_{0}r} (\mathbf{\hat{r}} \times \mathbf{p}_{n}) \times \mathbf{\hat{r}}\,e^{ik(r - \mathbf{\hat{r}}\cdot\mathbf{a}_{n}) - i(\omega t + \varphi_{n})}
```

```math
\Large
\mathbf{H}_\infty(\mathbf{r}, t) = \sum_{n=1}^{N}\frac{ck^2}{4\pi r} (\mathbf{\hat{r}} \times \mathbf{p}_{n}) e^{ik(r - \mathbf{\hat{r}}\cdot\mathbf{a}_n) - i(\omega t + \varphi_{n})}
```

``dipole.field.dipole_radiant_intensity`` computes the average power
radiated and is given by ([radiant intensity](https://en.wikipedia.org/wiki/Radiant_intensity))


```math
\Large
\begin{aligned}
  \frac{\partial \overline{P}}{\partial \Omega} &= 
  r^{2} |\langle S_{r} \rangle| = \frac{r^{2}}{2Z_0} \left|{\mathbf{E}_{\infty}}\right|^{2} \\
  &=\frac{k^{4}}{32\pi^{2}\epsilon_{0}^{2}Z_0} 
  \left|{\sum_{n=1}^{N} (\mathbf{\hat{r}} \times \mathbf{p}_{n}) \times \mathbf{\hat{r}}\, e^{- i(k\mathbf{\hat{r}} \cdot\mathbf{a}_{n} + \varphi_{n})}}\right|^{2} \\ 
  &= \frac{k^{4}}{32\pi^{2}\epsilon_{0}^{2}Z_0} 
  \left|{\sum_{n=1}^{N} (\mathbf{\hat{r}} \times \mathbf{p}_{n})\, e^{- i(k\mathbf{\hat{r}}\cdot\mathbf{a}_{n} + \varphi_{n})}}\right|^{2}
\end{aligned}
```

See the examples in `examples/*.py` and the unit tests in `dipole/tests/*.py` for examples on how to use the mentioned functions.

## API
```python
def dipole_radiant_intensity(
        np.ndarray[double_t, ndim=2] T,  # theta coords (observation points)
        np.ndarray[double_t, ndim=2] P,  # phi coords (observation points)
        np.ndarray[double_t, ndim=2] p,  # dipole moments
        np.ndarray[double_t, ndim=2] r,  # dipole positions
        np.ndarray[double_t, ndim=1] phases,
        double_t k):
    ...

def dipole_general(np.ndarray[double_t, ndim=3] r, # observation points
                   np.ndarray[double_t, ndim=2] P, # dipole moments
                   np.ndarray[double_t, ndim=2] R, # dipole positions
                   np.ndarray[double_t, ndim=1] phases,
                   double_t k, # wave number
                   bool poyntingmean=False, # TODO deprecate this kwarg
                   bool poyntingstatic=False, # TODO deprecate this kwarg
                   double_t t=0):
    ...

# computes E field in the far-field region
def dipole_e_ff(np.ndarray[double_t, ndim=3] r,
                np.ndarray[double_t, ndim=2] P,
                np.ndarray[double_t, ndim=2] R,
                np.ndarray[double_t, ndim=1] phases,
                double_t k, double_t t=0):
    ...

# computes H field in the far-field region
def dipole_h_ff(np.ndarray[double_t, ndim=3] r,
                np.ndarray[double_t, ndim=2] P,
                np.ndarray[double_t, ndim=2] R,
                np.ndarray[double_t, ndim=1] phases,
                double_t k, double_t t=0):
    ...

```

## References

[Notes by Alpar Sevgen](http://www.phys.boun.edu.tr/~sevgena/p202/docs/Electric%20dipole%20radiation.pdf)

[Electric Dipole Radiation (1)](https://en.wikipedia.org/wiki/Multipole_radiation#Electric_dipole_radiation)

[Electric Dipole Radiation (2)](https://en.wikipedia.org/wiki/Dipole#Dipole_radiation)

## Requirements
* Python 3
* Numpy
* Cython
* Matplotlib
* py.test 

## Compilation

    python setup.py build_ext -i

## Run unit tests

```
pytest
pytest --interactive  # unskip interactive tests (matplotlib plots are shown)
pytest --nocapturelog -s  # shows the log output
pytest 'dipole/tests/test_ring.py::TestRing::test_rolf_pishift[True]' --interactive  # run a single test
```
